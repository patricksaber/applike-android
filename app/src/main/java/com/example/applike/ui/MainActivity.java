package com.example.applike.ui;


import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;

import com.example.applike.R;
import com.example.applike.service.BackgroundService;

import timber.log.Timber;

public class MainActivity extends AppCompatActivity implements BackgroundService.ServiceCallbacks {
    private boolean isBound = false;
    private BackgroundService backgroundService;
    int time = 0;
    Runnable mRunnable;
    private boolean pauseTimer = false;
    String result;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent serviceIntent = new Intent(this, BackgroundService.class);
        updateDisplay();
        startService(serviceIntent);
        bindService(serviceIntent, serviceConnection, BIND_AUTO_CREATE);
        isBound = true;
    }


    private void updateDisplay() {
        final Handler handler = new Handler();

        mRunnable = new Runnable() {
            @Override
            public void run() {
                if (!pauseTimer) {
                    result = String.format("%02dm:%02ds", time / 60, time % 60);
                    time++;
                    handler.postDelayed(this, 1000);
                }
            }
        };
    }


    /**
     * Activity is destroyed, it should unbind from the service
     **/
    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (isBound) {
            backgroundService.unregisterActivity();
            unbindService(serviceConnection);
            isBound = false;
        }
    }

    /**
     * The service can call this method to update the lastUnlock TextView
     **/
    @Override
    public void updateClient(String data) {
        pauseTimer = true;
        Timber.i("updateClient" + data);
    }


    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            BackgroundService.LocalBinder binder = (BackgroundService.LocalBinder) service;
            backgroundService = binder.getServiceInstance();
            backgroundService.registerActivity(MainActivity.this);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBound = false;
            backgroundService = null;
        }
    };
}
