package com.example.applike;

import android.content.Context;
import android.content.res.Configuration;
import android.support.multidex.MultiDexApplication;

import com.example.applike.timber.ReleaseTree;

import java.util.Locale;

import timber.log.Timber;


/**
 * Created by abpa on 3/28/2018
 */
public class ApplicationContext extends MultiDexApplication {

    private static ApplicationContext instance;
    public static int AppTheme = R.style.AppTheme;

    public ApplicationContext() {
        instance = this;
    }

    public static Context getContext() {
        return instance;
    }


    @Override
    public void onCreate() {
        super.onCreate();
//        Fabric.with(this, new Crashlytics());
        instance.setTheme(AppTheme);
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree() {
                @Override
                protected String createStackElementTag(StackTraceElement element) {
                    return super.createStackElementTag(element) + ':' + element.getLineNumber();
                }
            });
        } else {
            Timber.plant(new ReleaseTree());
        }
        final Thread.UncaughtExceptionHandler oldHandler = Thread.getDefaultUncaughtExceptionHandler();

        Thread.setDefaultUncaughtExceptionHandler(
                new Thread.UncaughtExceptionHandler() {
                    @Override
                    public void uncaughtException(Thread paramThread, Throwable paramThrowable) {

						/*if(paramThrowable.getMessage()!=null)
                            logger.error("Exception :: "
	                	+ Methods.getPref(getApplicationContext(), Config.PREF_KEY_USER_ID)
	                	+"\n " + paramThrowable.getMessage().toString());*/
                        //Do your own error handling here
                        if (oldHandler != null)
                            //Delegates to Android's error handling
                            oldHandler.uncaughtException(paramThread, paramThrowable);
                        else
                            System.exit(2); //Prevents the service/app from freezing
                    }
                });





    }

    private Locale locale = null;

    public static int getAppTheme() {
        return AppTheme;
    }

    public static void setAppTheme(int appTheme) {
        AppTheme = appTheme;
        instance.setTheme(AppTheme);
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
      //  super.attachBaseContext(LocaleHelper.onAttach(base));

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
       // LocaleHelper.onAttach(this);
    }
}
