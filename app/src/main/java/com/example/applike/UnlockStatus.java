package com.example.applike;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class UnlockStatus
{
    private Calendar calendar;
    private final SimpleDateFormat SDF = new SimpleDateFormat("dd.MM.yyyy' kl. 'HH:mm:ss");
    private boolean unlockRegistered = false;

    public UnlockStatus()
    {
        calendar = Calendar.getInstance();
    }

    public String getStatus()
    {
        if (unlockRegistered)
           return SDF.format(calendar.getTime());

        else
            return null;
    }

    public void setStatus(Date date)
    {
        unlockRegistered = true;
        calendar.setTime(date);
    }
}