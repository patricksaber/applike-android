package com.example.applike.service;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;

import com.example.applike.R;
import com.example.applike.UserPresentReceiver;

import java.util.Random;

public class BackgroundService extends Service {
    ServiceCallbacks activity;
    private final IBinder LOCAL_BINDER = new LocalBinder();
    private UserPresentReceiver userPresentReceiver;
    SharedPreferences sharedPreferences;

    String ADMIN_CHANNEL_ID = "ADMIN_CHANNEL_ID";
    private NotificationManager notificationManager;

    /**
     * The service is created, receivers are registered here
     **/
    @Override
    public void onCreate() {
        super.onCreate();
        userPresentReceiver = new UserPresentReceiver();
        registerReceiver(userPresentReceiver, new IntentFilter("android.intent.action.USER_PRESENT"));
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    /**
     * An activity called startService() or the process was killed, returning START_STICKY
     * * which restarts the service and this method is called
     **/
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    /**
     * An activity just bound to the service
     **/
    @Override
    public IBinder onBind(Intent intent) {
        return LOCAL_BINDER;
    }

    /**
     * Service is destroyed, unregister the receivers
     **/
    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(userPresentReceiver);
    }


    public class LocalBinder extends Binder {
        public BackgroundService getServiceInstance() {
            return BackgroundService.this;
        }
    }

    /**
     * Update the activity if it is connected, and save the unlockStatus in the preferences
     **/
    public void updateActivity(String unlockStatus) {
        if (activity != null) {
            this.activity.updateClient(unlockStatus);
        }

        SharedPreferences.Editor spEditor = sharedPreferences.edit();
        spEditor.putString("saved_last_unlock", unlockStatus);
        sendNotification(unlockStatus);
        spEditor.apply();
    }

    /**
     * Methods an activity can call to register or unregister itself to the service
     **/
    public void registerActivity(Activity activity) {
        this.activity = (ServiceCallbacks) activity;

        // If the last unlock is saved in the preferences, retrieve it from there
        String savedLastUnlock = sharedPreferences.getString("saved_last_unlock", null);
        if (savedLastUnlock != null) {
            this.activity.updateClient(savedLastUnlock);
        }
    }

    public void unregisterActivity() {
        activity = null;
    }

    /**
     * An activity has to implement this interface so that the service can send commands to it
     **/
    public interface ServiceCallbacks {
        void updateClient(String data);
    }


    private void sendNotification(String message) {
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            setupChannels();
        }

        int notificationId = new Random().nextInt(60000);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, ADMIN_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true)
                .setColor(getResources().getColor(R.color.colorPrimary))
                .setSound(defaultSoundUri);


        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.notify(notificationId, notificationBuilder.build());
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupChannels() {
        CharSequence adminChannelName = "notifications_admin_channel_name";
        String adminChannelDescription = "notifications_admin_channel_description";

        NotificationChannel adminChannel;
        adminChannel = new NotificationChannel(ADMIN_CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_LOW);
        adminChannel.setDescription(adminChannelDescription);
        adminChannel.enableLights(true);
        adminChannel.setLightColor(Color.RED);
        adminChannel.enableVibration(true);
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(adminChannel);
        }
    }

}