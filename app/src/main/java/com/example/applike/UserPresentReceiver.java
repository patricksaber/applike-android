package com.example.applike;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.applike.service.BackgroundService;

import java.util.Date;

public class UserPresentReceiver extends BroadcastReceiver {
    private final String TAG = "UserPresentReceiver";
    UnlockStatus unlockStatus;

    public UserPresentReceiver() {
        unlockStatus = new UnlockStatus();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        unlockStatus.setStatus(new Date());

        BackgroundService service = (BackgroundService) context;
        service.updateActivity(unlockStatus.getStatus());
    }
}
