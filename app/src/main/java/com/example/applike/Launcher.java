package com.example.applike;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.applike.ui.MainActivity;
import com.example.applike.utils.Config;
import com.example.applike.utils.LocaleHelper;
import com.example.applike.utils.Methods;

import timber.log.Timber;


public class Launcher extends Activity {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!isTaskRoot()
                && getIntent().hasCategory(Intent.CATEGORY_LAUNCHER)
                && getIntent().getAction() != null
                && getIntent().getAction().equals(Intent.ACTION_MAIN)) {

            finish();
            return;
        }
        Timber.i("PUSH_ID===\n" + Methods.getPref(this, Config.PREF_KEY_FCM_PUSHID));
        switch (Methods.getPref(this, Config.PREF_KEY_REGISTRATION_STATUS)) {


            case Config.REGISTRATION_ALREADY_HAVE_PINCODE: //save number
                Intent intent = new Intent(this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                intent.putExtra(EnterNumberActivity.REG_STATUS, Config.REGISTRATION_ALREADY_HAVE_PINCODE);
                startActivity(intent);
                finish();
                break;

            default:
                startActivity(new Intent(this, MainActivity.class)/*.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK)*/);
                finish();
                break;

        }


    }
}
